use bindings::GeolocationPosition;

mod bindings;

use serde::{Serialize, Deserialize};
use wasm_bindgen::{JsValue, prelude::Closure, JsCast};
use yew::prelude::*;

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
#[serde(rename_all = "PascalCase")]
struct BusStop {
    bus_stop_code: String,
    road_name: String,
    description: String,
    latitude: f32,
    longitude: f32 
}

#[derive(Debug, Deserialize, Clone, PartialEq)]
#[serde(rename_all = "camelCase")]
struct Closest {
    bus_stop: BusStop,
    distance: f64 
}

fn main() {
    yew::start_app::<Root>();
    println!("Hello, world!");
}


#[function_component(BusContainer)]
fn bus_container() -> Html {
    let current = use_state_eq(|| Closest{
        bus_stop: BusStop{
        bus_stop_code: "000000".to_string(),
        road_name: "Loading".to_string(),
        description: "".to_string(),
        latitude: 0.0,
        longitude: 0.0,
        },
        distance: 0.0,
    });

    let window = web_sys::window().expect("no global `window` exists");
    let geolocation = window
    .navigator()
    .geolocation()
    .expect("Unable to get geolocation.");

    let geo_callback = move |position:  JsValue| {
        let pos: GeolocationPosition = position.into();
        let coords = pos.coords();

        wasm_bindgen_futures::spawn_local(async move {
            let response = reqwest::get(format!("https://8080-seeya-bustimingapi-2h3qw5k297y.ws-us71.gitpod.io/?latitude={:}&longitude={:}", coords.latitude(), coords.longitude())).await.unwrap();
            let closest= response.json::<Closest>().await.unwrap(); 

            // let c = current.clone();
            // Callback::from(move |_| current.set(closest));
          
        })
    };

    let geo_callback_function = Closure::wrap(Box::new(geo_callback) as Box<dyn FnMut(JsValue)>);

    let onclick = Callback::from(move | mouse_event: MouseEvent | {
        geolocation.get_current_position(geo_callback_function.as_ref().unchecked_ref()).expect("Cannot get position");
    });


    html! {
        <div>
            <button {onclick}>{"🚌"}</button>
        </div>
    }
}


#[function_component(Root)]
fn root() -> Html {
    html! {
        <div>
            <BusContainer/>
        </div>
    }
}
